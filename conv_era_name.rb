require 'erb'
require 'yaml'

module ConvEraName
  def ad2jp(year)
    table = load_table
    
    buf = year
    
    table.keys.sort.each do |start_year|
      break if(start_year > year)
      buf = start_year
    end
    
    era = table[buf]
    jp_year = year - buf + 1

    era + (jp_year == 1 ? '元' : jp_year) + '年'
  end
  
  def load_table
    @@table ||= YAML.load(ERB.new(IO.read('table.yml')).result)
  end
end
